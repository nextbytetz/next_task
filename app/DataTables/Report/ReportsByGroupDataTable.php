<?php

namespace App\DataTables\Report;

use App\Models\System\Report;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class ReportsByGroupDataTable extends DataTable
{


    protected $query;
    protected $input;



    public function __construct(){

    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())
            ->editColumn('report_type_id', function($report) {
                return $report->reportType->name;
            })
            ->editColumn('report_group_id', function($report) {
                return $report->reportGroup->name;
            });
    }






    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $this->query = Report::query()->where('report_group_id', $this->group_id)->orderBy('name');


//dd($this->query->get());
        return $this->applyScopes($this->query);
    }



    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                     document.location.href = '". url("/") . "/admin/reports/' + aData['url_name'] ;
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('label.name'), 'orderable' => true, 'searchable' => true],
            ['data' => 'report_type_id', 'name' => 'report_type_id', 'title' => trans('label.administrator.system.reports.report_type'), 'orderable' => false, 'searchable' => false],
            ['data' => 'report_group_id', 'name' => 'report_group_id', 'title' => trans('label.administrator.system.reports.report_group'), 'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reports' . date('YmdHis');
    }
}
