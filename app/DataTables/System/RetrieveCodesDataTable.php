<?php

namespace App\DataTables\System;


use App\Repositories\System\CodeRepository;
use App\Repositories\System\CodeValueRepository;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class RetrieveCodesDataTable extends DataTable
{


    protected $query;
    protected $input;
    protected $codes;



    public function __construct(){
        $this->codes = new CodeRepository();
    }




    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return DataTables::of($this->query())
            ->editColumn('is_system_defined', function($code) {
                return $code->system_defined;
            })
            ->addColumn('action_buttons', function($code) {
                return $code->edit_button;
            })
            ->rawColumns(['action_buttons']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $codes = new CodeRepository();
        $query = $codes->query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
//                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                    $(nRow).click(function() {
//                        document.location.href = '". url("/") . "/code/values/' + aData['id'];
//                    }).hover(function() {
//                        $(this).css('cursor','pointer');
//                    }, function() {
//                        $(this).css('cursor','auto');
//                    });
//            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('label.name')],
            ['data' => 'is_system_defined', 'name' => 'is_system_defined', 'title' => trans('label.administrator.system.codes.system_defined'),'orderable' => false, 'searchable' => false],
            ['data' => 'action_buttons', 'name' => 'action_buttons', 'title' => trans('label.action'),'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'codes_' . time();
    }
}
