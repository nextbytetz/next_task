<?php

namespace App\DataTables\System;

use App\Repositories\System\CodeRepository;
use App\Repositories\System\SysdefGroupRepository;
use App\Repositories\System\SysdefRepository;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class RetrieveSysDefinitionsDataTable extends DataTable
{


    protected $query;
    protected $input;
    protected $sysdefs;

    public function __construct(){
        $this->sysdefs = new SysdefRepository();
    }

    /**
     * Build DataTable class.
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        return DataTables::of($this->query()->where('sysdef_group_id', $this->sysdef_group_id))
            ->addColumn('action_buttons', function($sysdef) {
                return $sysdef->edit_button;
            })
            ->editColumn('value', function ($sysdef) {
                return truncateString($sysdef->value, 200);
            })
            ->rawColumns(['action_buttons', 'value']);

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = $this->sysdefs->query()->where('isactive', 1);
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
                //'dom' => 'Bfrtip',
                'searching' => true,
                /*'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/code/values/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",*/
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'display_name', 'name' => 'display_name', 'title' => trans('label.name')],
            ['data' => 'value', 'name' => 'value', 'title' => trans('label.value'),'orderable' => false, 'searchable' => false],
            ['data' => 'data_type', 'name' => 'data_type', 'title' => trans('label.administrator.system.sysdef.data_type'),'orderable' => false, 'searchable' => false],
            ['data' => 'action_buttons', 'name' => 'action_buttons', 'title' => trans('label.action'),'orderable' => false, 'searchable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'codes_' . time();
    }
}
