<?php

namespace App\DataTables\System;

use App\Repositories\System\CodeRepository;
use App\Repositories\System\SysdefGroupRepository;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class RetrieveSysdefGroupsDataTable extends DataTable
{


    protected $query;
    protected $input;
    protected $sysdef_groups;



    public function __construct(){
        $this->sysdef_groups = new SysdefGroupRepository();
    }




    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return DataTables::of($this->query());

    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = $this->sysdef_groups->query();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->parameters([
//                'dom' => 'Bfrtip',
                'searching' => true,
                'rowCallback' => "function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).click(function() {
                        document.location.href = '". url("/") . "/admin/sysdef/definitions/' + aData['id'];
                    }).hover(function() {
                        $(this).css('cursor','pointer');
                    }, function() {
                        $(this).css('cursor','auto');
                    });
            }",
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'name', 'name' => 'name', 'title' => trans('label.name')],
            ['data' => 'description', 'name' => 'description', 'title' => trans('label.description'),'orderable' => false, 'searchable' => false],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'codes_' . time();
    }
}
