<?php

namespace App\Jobs\Payment;

use App\Repositories\Application\ApplicationRepository;
use App\Repositories\Premise\PremiseApplicationRepository;
use App\Repositories\Product\ProductApplicationRepository;
use App\Services\Payment\InvoiceCreatorService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class InvoiceCreator implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {

    }


    /**
     *
     * Execute the job.
     * @throws \App\Exceptions\GeneralException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        /*
         * Initialize Product Applications
         * */
        $product_applications = (new ProductApplicationRepository())->getAllHavePayment();
        foreach ($product_applications as $application) {
            (new InvoiceCreatorService())->verify($application->id, $application->wf_module_id, $application->number);
        }

        /*
         * Initialize Premise Applications
         * */
        $premise_applications = (new PremiseApplicationRepository())->getAllHavePayment();
        foreach ($premise_applications as $application) {
            (new InvoiceCreatorService())->verify($application->id, $application->wf_module_id, $application->number, 'premise_applications');
        }

        /*
         * Initialize Import Applications
         * */
        $applications = (new ApplicationRepository())->getAllHavePayment();
        foreach ($applications as $application) {
            (new InvoiceCreatorService())->verify($application->id, $application->wf_module_id, $application->number,"applications");
        }
    }
}
