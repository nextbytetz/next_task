<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            [
                'member.company.dashboard',
                'member.company.application_option',
                'member.company.application_pvoc',
                'member.company.application_di',
                'member.company.importer_profile',
                'application.option',
                'application.forms.application_form',
                'application.processed',
                'application.completed',
                'application.view',
                'member.importer.importer',
                'member.importer.registration_form',
                'member.importer.profile',
                'member.importer.edit_form',
                'application.forms.search_bl_number_form',
                'application.forms.apply',
                'profile.view',
                'member.company.forms.edit',
                'member.director.forms.edit',
                'includes.member.company._company_details',
                'premise.form.registration_form',
            ],
            'App\Http\View\Composers\Member\Company\CompanyComposer'
        );

        View::composer(
            [
                '*'
            ],
            'App\Http\View\Composers\Wftrack\WfTrackComposer'
        );



        View::composer(
            [
                '*'
            ],
            'App\Http\View\Composers\District\DistrictComposer'
        );



    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
