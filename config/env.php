<?php
/**
 * Created by PhpStorm.
 * User: nextbyte_admin
 * Date: 9/14/18
 * Time: 9:09 AM
 */

return [
    "key" => "PscUNdlxRk8Q3nwM",
    "app" => [
        "name" => "NEXT PROJECT",
        "company" => "NEXTBYTE | NEXTPROJECT",
        "vendor" => "NEXTBYTE ICT SOLUTIONS CO LTD",
        "vendor_web" => "https://www.nextbyte.co.tz",
        "client_web" => "https://nextproject.go.tz",
        "description" => "NEXTBYTE | BOILERPLATE",
        "keywords" => "",
    ],
];
