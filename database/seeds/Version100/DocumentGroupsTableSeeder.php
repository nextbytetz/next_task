<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentGroupsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys("document_groups");
        $this->delete('document_groups');

        \DB::table('document_groups')->insert(array (
//            0 =>
//                array (
//                    'id' => 1,
//                    'name' => 'Company Registration',
//            'top_path' => '/storage/stakeholder/company',
//            'top_dir' => '/stakeholder/company',
//                ),
//

        ));
        $this->enableForeignKeys("document_groups");

    }
}
