<?php

use Illuminate\Database\Seeder;
use Database\TruncateTable;
use Database\DisableForeignKeys;

class DocumentsTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        $this->disableForeignKeys("documents");
        $this->delete('documents');

        \DB::table('documents')->insert(array (
//            0 => array (
//                    'id' => 1,
//                    'name' => 'TIN Certificate',
//                    'document_group_id' => 1,
//                    'description' => 'This is The Tin Certificate of Registered Company',
//                    'isrecurring' => 0,
//                    'ismandatory' => 1,
//                    'isrenewable' => 1,
//                    'isactive' => 1,
//                ),



        ));

        $this->enableForeignKeys("documents");

    }
}
