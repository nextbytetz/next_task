

/* start ---Enable - Disable attributes of elements*/

/*Work for element id*/
function enable_disable(action_type, element_id)
{
    switch (action_type) {
        case 'enable_id':
            $("#"+element_id).prop("disabled", false);
            break;
        case 'disable_id':
            $("#"+element_id).prop("disabled", true);
            break;
        case 'enable_class':
            $("."+element_id).prop("disabled", false);
            break;
        case 'disable_class':
            $("."+element_id).prop("disabled", true);
            break;
    }
}

/*End - Enable_Disable*/

/* start ---HIDE - SHOW attributes of elements*/

/*Work for element id*/
function hide_show(action_type, element_id)
{
    switch (choice) {
        case 'hide_id':
            $("#" + element_id).hide();
            break;
        case 'show_id':
            $("#"+ element_id).show();
            break;
        case 'hide_class':
            $("." + element_id).hide();
            break;
        case 'show_class':
            $("."+ element_id).show();
            break;
    }
}

/*End - hide_show*/




/* start ---CHECKED - UNCHECKED attributes of elements*/

/*Work for element id*/
function checker(action_type, element_id)
{
    switch (choice) {
        case 'check_id':
            $("#"+element_id).prop("checked", true);
            break;
        case 'uncheck_id':
            $("#"+element_id).prop("checked", false);
            break;
        case 'check_class':
            $("."+element_id).prop("checked", true);
            break;
        case 'uncheck_class':
            $("."+element_id).prop("checked", false);
            break;
    }
}

/*End - checker*/




/**
 * INITIALIZE VALUES FOR PLUGINS ---=========Start=========----
 */




/* start ----Maskmoney -----*/

/* start : mask all money input */
$('.money').maskMoney({
    precision : 2,
    allowZero : false,
    affixesStay : false
});

$('.money0').maskMoney({
    precision : 2,
    allowZero : true,
    affixesStay : false
});


/*--End ----Maskmoney---------*/



/*------Start------Number Only ------*/

//for number check
$('body').off('keydown', '.number').on('keydown', '.number', function(e) {
    number_only(e);
});

function number_only(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}


/*------End--------Number only---------*/



/*End initialize values -----===========-----------*/