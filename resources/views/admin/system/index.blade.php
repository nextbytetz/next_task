@extends('layouts.main', ['title' => trans('label.system'), 'header' => trans('label.system')])

@include('includes.validate_assets')

@section('content')

    @include('admin/system/menu')

@endsection
