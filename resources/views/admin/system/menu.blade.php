<div class="row">
    <div class="col-md-4">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{{ route('code.index') }}" class="list-group-item list-group-item-action">
                    <h5 class="list-group-item-heading"><i class="fas fa-list"></i> @lang('label.administrator.system.code_values.code_values')</h5>
                    <p class="list-group-item-text">@lang('label.administrator.system.code_values.manage_code_values')</p>
                </a>
            </ul>
        </div>
    </div>


    <div class="col-md-4">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{{ route('sysdef.index') }}" class="list-group-item list-group-item-action">
                    <h5 class="list-group-item-heading"><i class="fas fa-cogs"></i> @lang('label.administrator.system.sysdef.definitions')</h5>
                    <p class="list-group-item-text">@lang('label.administrator.system.sysdef.manage')</p>
                </a>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{{ route('access.role.index') }}" class="list-group-item list-group-item-action">
                    <h5 class="list-group-item-heading"><i class="fas fa-key"></i> @lang('label.administrator.system.access_control.roles_permissions')</h5>
                    <p class="list-group-item-text">@lang('label.administrator.system.access_control.manage_roles_permissions')</p>
                </a>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{{ route('country.index') }}" class="list-group-item list-group-item-action">
                    <h5 class="list-group-item-heading"><i class="fas fa-plus-square"></i> @lang('label.country')</h5>
                    <p class="list-group-item-text">@lang('label.administrator.system.country.manage')</p>
                </a>
            </ul>
        </div>
    </div>


    <div class="col-md-4">
        <div class="list-group">
            <ul class="list-unstyled">
                <a href="{{ route('system.audit.search_page') }}" class="list-group-item list-group-item-action">
                    <h5 class="list-group-item-heading"><i class="fas fa-search-plus"></i> @lang('label.administrator.system.audits.manage_audits')
                    </h5>
                    <p class="list-group-item-text">@lang('label.administrator.system.audits.search_audits_descr')</p>
                </a>
            </ul>
        </div>
    </div>

</div>



