<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "allocation-workflow-table" cellspacing="0" width="100%">
            <thead>
            <tr >
                <th></th>
                <th>@lang('label.resource')</th>
                {{--<th>@lang('labels.backend.system.workflow.wf_date')</th>--}}
                <th>@lang('label.received_date')</th>
                <th>@lang('label.level')</th>
                {{--<th>@lang('labels.backend.system.workflow.description')</th>--}}
                <th>@lang('label.module')</th>
                {{--<th>@lang('labels.backend.system.workflow.module')</th>--}}
                <th>@lang('label.status')</th>
                <th>@lang('label.user')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>