<div class="row">
    <div class="col-md-12 col-sm-12">
        <table class="display" id = "pending-workflow-table" cellspacing="0" width="100%">
            <thead>
            <tr >
                <th>@lang('label.resource_name')</th>
                {{--<th>@lang('labels.backend.system.workflow.wf_date')</th>--}}
                <th>@lang('label.received_date')</th>
                <th>@lang('label.level')</th>
                <th>@lang('label.description')</th>
                <th>@lang('label.module')</th>
                {{--<th>@lang('labels.backend.system.workflow.module')</th>--}}
                <th>@lang('label.status')</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
