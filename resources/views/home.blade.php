@extends('layouts.main', ['title' => __("label.home"), 'header' => __("label.home")])

@push('after-styles')
    {{ Html::style(url('vendor/select2/css/select2.min.css')) }}
    <style>

    </style>
@endpush

@section('content')

    @guest
        <div class="row">
            <div class="col-sm-8">
    <section class="card">
        <div class="card-body">
        {{--@include("includes/carousel")--}}
        <h1><center>@lang('label.greetings') <img src="{{ url("img/np_logo3.gif") }}" width="120" height="45" alt="{{ config("app.name") }}" /></center></h1>
        <hr>
                    <ul>
                        <li>NextProject is Nextbyte Boilerplate laravel project which contains all system features and pre defined data which may be applicable to most of the future laravel projects. Put steps ahead with this project to speed up development implementation of projects. This boilerplate is currently on Laravel 6.</li>
      
                        <li>The NextProject will enable Nextbyte developers to have the following features ready-made;
                        <ol>
                            <li>Authentication</li>
                        <li>System Migrations and Seeders </li>
                        <li>System Routes </li>
                        <li>System Controllers</li>
                        <li>System Features</li>
                            <li>Workflow</li>
                            <li>And many more </li>

                        </li>
                        </ol>
                    </ul>
          
        </div>
    </section>
    
            </div>
            
    @include('auth/includes/home_login')
    
        </div>

    @endguest




    @auth

        {{--@include('includes.navigation.menu');--}}

                {{--@if (access()->user()->isAdmin())--}}

                {{--@elseif (access()->user()->isContactPerson())--}}


                            {{--@if (access()->user()->agent()->get()->isEmpty())
                                @include('includes.contact_person.welcome_new_contact_person')
                            @else--}}





                                {{--<div class="row">
                                    <div class="col-md-9">

                                            <section class="card">
                                                <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                                    </div>

                                                    <h2 class="card-title">Tooltips</h2>
                                                    <p class="card-subtitle">Easily show tooltips with a few html attributes.</p>
                                                </header>
                                                <div class="card-body">
                                                    <button type="button" class="btn btn-primary m-xs" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Top</button>
                                                    <button type="button" class="btn btn-primary m-xs" data-toggle="tooltip" data-placement="right" title="Tooltip on right">Right</button>
                                                    <button type="button" class="btn btn-primary m-xs" data-toggle="tooltip" data-placement="left" title="Tooltip on left">Left</button>
                                                    <button type="button" class="btn btn-primary m-xs" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">Bottom</button>
                                                    <a data-toggle="tooltip" title="Tooltip Link" href="#">Link</a>
                                                </div>
                                            </section>

                                    </div>

                                    <div class="col-md-3">


                                        --}}{{--start--}}{{--

                                        <!-- start: page -->

                                            --}}{{--{{ access()->user()->agent()->get()  }}--}}{{--


                                                    <section class="card">
                                                        <div class="card-body">

                                                            <h2 class="card-title">
                                                                <span class="va-middle">Friends</span>
                                                            </h2>

                                                            <hr class="dotted short">

                                                            <h5 class="mb-2 mt-3">TIN Number</h5>
                                                            <p class="text-2">malesuada</p>
                                                            <hr class="dotted short">
                                                            <h5 class="mb-2 mt-3">TIN Number</h5>
                                                            <p class="text-2">malesuada</p>
                                                            <hr class="dotted short">
                                                            <h5 class="mb-2 mt-3">TIN Number</h5>
                                                            <p class="text-2">malesuada</p>
                                                            <hr class="dotted short">
                                                            <div class="clearfix">
                                                                <a class="text-uppercase text-muted float-right" href="#">(View All)</a>
                                                            </div>

                                                    </section>


                                                    --}}{{--end--}}{{--


                                    </div>

                                </div>--}}

                                    {{--@endif--}}

                    {{--@include("member.contact_person.dashboard")--}}


                {{--@endif--}}

        {{--<div class="row">
            <div class="col-lg-12">
                <p class="mx-auto">
                    Welcome, Thanks For Register to TBS Online System. Click Dashboard To continue
                </p>
            </div>
        </div>--}}



    @endauth

@endsection
