<!DOCTYPE html>
@auth
    <html class="fixed sidebar-left-sm">
    {{--<html class="fixed">--}}
    {{-- sidebar-left-collapsed  sidebar-left-big-icons left-sidebar-panel --}}
    @endauth

    @guest
        <html class="fixed has-top-menu">
        {{--<html class="fixed">--}}
        @endguest
        <head>
            {{--<!-- Basic -->--}}
            <meta charset="UTF-8">

            <title>{{ config("app.name") . " - " . $title }}</title>
            <meta name="keywords" content="{{ config("env.app.keywords") }}" />
            <meta name="description" content="{{ config("env.app.description") }}">
            <meta name="author" content="{{ config("env.app.vendor") }}">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <meta name="csrf-token" content="{{ csrf_token() }}">
            {{ Html::style(url("img/np_fav.png"), ['rel' => 'stylesheet icon', 'type' => 'image/x-icon']) }}

            @stack('before-styles')

            {{ Html::style(url("/css/fonts.googleapis.css"), ['rel' => 'stylesheet', 'type' => 'text/css']) }}

            {{ Html::style(url('vendor/bootstrap/css/bootstrap.min.css')) }}
            {{ Html::style(url('vendor/animate/animate.css')) }}
            {{ Html::style(url('vendor/font-awesome/css/fontawesome-all.min.css')) }}
            {{ Html::style(url('vendor/magnific-popup/magnific-popup.css')) }}
            {{ Html::style(url('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')) }}
            {{ Html::style(url("assets/nextbyte/plugins/jquery-ui/css/jquery-ui.min.css"), ['rel' => 'stylesheet']) }}
            @stack('after-styles')
            {{ Html::style(url('css/theme.css')) }}
            {{ Html::style(url('css/theme-elements.css')) }}
            {{ Html::style(url('css/skins/default.css')) }}
            {{ Html::style(url('css/custom.css')) }}
            {{ Html::style(url('vendor/select2/css/select2.min.css')) }}

            {!! Html::script(url('vendor/modernizr/modernizr.js')) !!}

            {{--STart notification css--}}
            {{--{{ Html::style(asset_url() . "/nextbyte/plugins/AmaranJS/dist/css/amaran.min.css") }}--}}
            {{--{{ Html::style(asset_url() . "/nextbyte/plugins/AmaranJS/dist/css/animate.min.css") }}--}}
            {{--end notification css--}}
            @stack('custom')

            <style>
                .required_asterik:after {
                    content: '*';
                    color: red;
                    padding-left: 5px;
                }
                .hidden_fields {
                    display: none;
                }
            </style>


        </head>
        <body>
        <section class="body" >

            @include("includes/components/header")

            <div class="inner-wrapper">

                @auth
                    @include('includes/components/left_sidebars/index')
                @endauth

                <section role="main" class="content-body">


                    {{--Hide header on home page--}}
                    <header  class="page-header " style="background-color:#32464a">
                        <h2>{{ $header }}</h2>
                        <div style="margin-right: 10px"  class="right-wrapper text-right">
                            {!! Breadcrumbs::render() !!}
                        </div>
                    </header>


                    {{--@include("includes/ads/top_advert")--}}

                    @include("includes.partials.messages")

                    @yield('content')



                </section>


            </div>
            {{--@guest--}}
            @include("includes/components/footer")
            {{--@endguest--}}



        </section>


        <script>
            var base_url = "{!! url("/") !!}";
        </script>
        {{--<!-- Scripts -->--}}
        @stack('before-scripts')
        {!! Html::script(url('vendor/jquery/jquery.js')) !!}
        {!! Html::script(url('assets/nextbyte/plugins/jquery-ui/js/jquery-ui.min.js')) !!}
        {!! Html::script(url('vendor/jquery-browser-mobile/jquery.browser.mobile.js')) !!}
        {!! Html::script(url('vendor/popper/umd/popper.min.js')) !!}
        {!! Html::script(url('vendor/bootstrap/js/bootstrap.min.js')) !!}
        {!! Html::script(url('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')) !!}
        {!! Html::script(url('vendor/common/common.js')) !!}
        {!! Html::script(url('vendor/nanoscroller/nanoscroller.js')) !!}
        {!! Html::script(url('vendor/magnific-popup/jquery.magnific-popup.min.js')) !!}
        {!! Html::script(url('vendor/jquery-placeholder/jquery-placeholder.js')) !!}
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bdc6737afad5b00117c870d&product=inline-share-buttons' async='async'></script>
        @stack('after-scripts')
        {!! Html::script(url('js/theme.js')) !!}
        {{--{!! Html::script(url('js/custom.js')) !!}--}}
        {!! Html::script(url('js/theme.init.js')) !!}
        {!! Html::script(url('vendor/select2/js/select2.min.js')) !!}
        {!! Html::script(url('js/share.js')) !!}
        {!! Html::script(url('assets/nextbyte/plugins/maskmoney/js/maskmoney.min.js')) !!}
        {!! Html::script(url('vendor/jquery-maskedinput/jquery.maskedinput.js')) !!}
        {!! Html::script(url('assets/nextbyte/js/custom.js')) !!}
        {{--STart - Notification--}}
        {{--{{ Html::script(asset_url(). "/global/plugins/AmaranJS/dist/js/jquery.amaran.min.js") }}--}}
        {{--End notification--}}
        <script>
            $(document).ready(function () {

                $('.mobile').mask("9999999999");

            })
        </script>

        </body>
        </html>
