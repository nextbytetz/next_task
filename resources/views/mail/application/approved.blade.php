<div width="100%" style="margin:0px; background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #32464a;">
    <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
            <tbody>
            <tr>
                <td style="vertical-align: top; padding-bottom:30px;" align="center">
                    <a href="{{ config("env.app.client_web") }}" target="_blank">
                        <img src="http://nextbyte.co.tz/tbs_logo.jpg" alt="TBS Logo" style="border:none">
                        <br>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="padding: 40px; background: #fff;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td class="col-md-6">
                    <b>{{ $name }},</b>
                    <p>Certificate of Conformity (COC) number  <b>{{ $coc }}</b></p>
                    <p>with Bill of Lading (BL) number  <b>{{ $bl }}</b> </p>
                    <p>is verified {{--, thus the consignment is approved--}} by TBS</p>
                        <p>@lang("strings.application.status")</p>
                        <center>
                            <a class="col-md-6" href="{{ route("application.view", $application) }}" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #263238; border-radius: 60px; text-decoration:none;"> @lang("label.application_status") </a>
                        </center>

                        <b>@lang("label.thanks") ( {{ config("env.app.name") }} )</b>
                    </td>
                    <td class="col-md-6" align="center">
                        {{--<img src="http://nextbyte.co.tz/approvedstamp.png" style="max-width: 125%">--}}
                    </td>
                </tr>
                <br>
                <tr>
                    <td class="col-md-4">
                        <p>@lang("strings.email.confirm_account.line_2")<a href="{{ route("application.view", $application) }}">{{ route("application.view", $application) }}</a></p>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
